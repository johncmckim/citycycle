var geo = (function() {

	var watchId = null;
	var lastLocation = null;
	var subscribers = {};

	var watchOptions = {
		maximumAge: 60000, // 1 minutes,
		timeout: 60000 // 1 minute
	}

	var locationChange = function(position) {
		lastLocation = position;

		for(key in subscribers) {
			_publish(key, lastLocation);
		}
	}

	var locationError = function(error) {
		//do something
	}

	var getLocation = function(callback) {
		if(!lastLocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				callback(position)
			});
		}

		callback(lastLocation);
	}

	var getLastLocation = function () {
	    return lastLocation;
	}

	var watchLocation = function(name, callback, context) {
		if(!name) return;
		if(typeof callback !== 'function') return;

		subscribers[name] = {
			callback: callback,
			context: context	
		};

		if(lastLocation) {
			_publish(name, lastLocation);
		};
	}

	var _publish = function(name, location) {
		var obj = subscribers[name];
		if(!obj.context) obj.context = window;
		
		obj.callback.call(obj.context, location);
	}

	var setAtQUT = function () {
	    var position = {
	        coords: {
	            latitude: -27.477683,
	            longitude: 153.028248
	        }
        };

	    locationChange(position);
	}

	var init = function() {
		watchId = navigator.geolocation.watchPosition(locationChange, locationError, watchOptions)
	}

	init();
	return {
	    getLocation: getLocation,
	    getLastLocation: getLastLocation,
	    watchLocation: watchLocation,
        setAtQUT: setAtQUT
	}
}());