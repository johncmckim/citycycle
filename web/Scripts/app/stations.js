﻿var stations = (function () {
    
    var distanceMatrix = null;
    var stations = [];  
    var markers = [];
    var markersVisible = true;
    var infoWindow = null;


    var toggleStationMarkers = function () {
        markersVisible = !markersVisible;
        return setStationsVisible(markersVisible);
    }

    var setStationsVisible = function (visible) {
        if (!markers.length) return;

        markersVisible = visible;

        for (var i = 0; i < markers.length; ++i) {
            markers[i].setVisible(markersVisible);
        }

        return markersVisible;
    }

    var addMarkers = function (map) {
        markers = [];
        markersVisible = true;

        for (var i = 0; i < stationData.length; ++i) {
            var station = stationData[i];

            var markerOptions = {
                position: new google.maps.LatLng(station.lat, station.lng),
                map: map,
                title: station.name
            };

            var marker = new google.maps.Marker(markerOptions);
            addInfoWindowEvent(marker, map);

            markers.push(marker);
        }

        google.maps.event.addListener(map, 'click', function () {
            if (infoWindow) {
                infoWindow.close();
            }
        });
    }

    var addInfoWindowEvent = function (marker, map) {

        google.maps.event.addListener(marker, 'click', function () {
            if (!infoWindow) infoWindow = new google.maps.InfoWindow();

            var content = '<h2>' + marker.getTitle() + '</h2>';
            content += '<a href="#" class="toHere" data-lat="' + marker.position.lat() + '" data-lng="' + marker.position.lng() + '">Directions To Here</a>';
            
            infoWindow.setContent(content);
            infoWindow.open(map, marker);
        });
    }

    var findClosest = function (lat, lng, callback) {
        var origins = [];
        origins.push(new google.maps.LatLng(lat, lng));

        var closeStations = filterStations(lat, lng);

        var request = {
            origins: origins,
            destinations: closeStations,
            travelMode: google.maps.TravelMode.WALKING,
        }

        distanceMatrix.getDistanceMatrix(request, function(response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK) {
                var origins = response.originAddresses; // -- ONLY 1
                var destinations = response.destinationAddresses;

                var closest = null;

                for (var i = 0; i < origins.length; i++) {
                    var results = response.rows[i].elements;

                    for (var j = 0; j < results.length; j++) {
                        var element = results[j];

                        var from = origins[i];
                        var to = destinations[j];

                        var duration = element.duration.value;

                        if (!closest || closest.duration > duration) {
                            closest = {
                                duration: duration,
                                duration_text: element.duration.text,
                                location: to
                            }
                        }
                    }
                }

                callback(closest);
            }
        });
    }

    var rad = function (x) {
        return x * Math.PI / 180;
    }

    var filterStations = function (lat, lng) {
        var R = 6371;
        var distances = [];

        for (i = 0; i < stations.length; i++) {
            var slat = stations[i].lat();
            var slng = stations[i].lng();
            
            var dLat = rad(slat - lat);
            var dLong = rad(slng - lng);

            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);

            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;

            distances[i] = {
                station: stations[i],
                distance: d
            };
        }

        distances.sort(function (a, b) {
            return a.distance - b.distance;
        });
        
        var closest = distances.slice(0, 10);

        return _.pluck(closest, 'station');
    }

    var initGeo = function () {
        distanceMatrix = new google.maps.DistanceMatrixService();
        
        for (i = 0; i < stationData.length; i++) {
            stations.push(new google.maps.LatLng(stationData[i].lat, stationData[i].lng));
        }
    }


    var loadData = function () {
        /*
        UNFORTUNATELY NOT ALLOWED 
        $$.get('http://www.citycycle.com.au/service/carto', function (data) {
            console.log(data);
        }, 'xml');
        */
    }

    return {
        initGeo: initGeo,
        findClosest: findClosest,
        addMarkers: addMarkers,
        toggleMarkers: toggleStationMarkers,
        setMarkersVisible: setStationsVisible
    }
}());

/* OLD METHOD


*/