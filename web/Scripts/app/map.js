var map = (function() {
	
	var options = {
		center: null,
		zoom: 1,
		mapTypeId: null
	}

	var location_options = {
    	strokeColor: "#0F4DA8",
	    strokeOpacity: 0.8,
	    strokeWeight: 2,
	    fillColor: "#437DD4",
	    fillOpacity: 0.35,
	    radius: 5
    };

	var _gmap = null;
	var _directionsDisplay = null;
	var _position = null;
	var _location_circle = null;
	var _defaultLocation = null;

	var load = function() {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDVlgK0W8nOoEg7lYn2qOkxyrhgc0B7E1o&sensor=true&callback=map_init";
		document.body.appendChild(script);
	}

	var locationChange = function(position) {
		_position = position;
		var center = new google.maps.LatLng(_position.coords.latitude, _position.coords.longitude);
		_gmap.setCenter(center);
		_gmap.setZoom(17);


		if(!_location_circle) {
			location_options.center = center;
			location_options.map = _gmap;
			_location_circle = new google.maps.Circle(location_options);
		} else {
			_location_circle.setCenter(center);
		}
	}

	var init = function() {
		_defaultLocation = new google.maps.LatLng(-24.44715, 133.857422);

		options.mapTypeId = google.maps.MapTypeId.ROADMAP;
		options.center = _defaultLocation;

		_gmap = new google.maps.Map(document.getElementById("map"), options);
		_directionsDisplay = new google.maps.DirectionsRenderer();
		_directionsDisplay.setMap(_gmap);

		stations.addMarkers(_gmap);

		geo.watchLocation('map', locationChange, this);
	}

	var showDirections = function (directions) {
	    _directionsDisplay.setDirections(directions);
	}

	load();
	return {
	    init: init,
	    showDirections: showDirections
	}
}());


function map_init() {
    map.init();
    stations.initGeo();
	App.Data.initGeo();
} 