App.Data = (function(lng, app, undefined) {

    var directionsService = null;
    var geocoder = null;
    var bounds = null;
    

	var getSuggestions = function(text, callback) {
	    var request = {
	        address: text,
	        bounds: bounds,
	        region: 'AU'
        }

	    geocoder.geocode(request, function(results, status) {
	    	if (status == google.maps.GeocoderStatus.OK) {
	    	    var filtered = _.filter(results, function (result) {
	    	        return bounds.contains(result.geometry.location);
	    	    });
                
	    	    callback(filtered);
	    	} else {
	    		console.log(status);
	    	}
	    });
	}

	var getDirections = function (from, to, waypoints, callback) {
	    var origin = new google.maps.LatLng(from.lat, from.lng);
	    var destination = new google.maps.LatLng(to.lat, to.lng);

	    var request = {
	        origin: origin,
	        destination: destination,
	        //travelMode: google.maps.TravelMode.BICYCLING, --- WOULD BE NICE TO USE THIS
	        travelMode: google.maps.TravelMode.DRIVING,
	        unitSystem: google.maps.UnitSystem.METRIC,
	        region: 'AU',
	        waypoints: waypoints
	    }

	    directionsService.route(request, function (result, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            callback(result);
	        }
	    });
	}

	var initGeo = function() {
	    geocoder = new google.maps.Geocoder();

	    var ne = new google.maps.LatLng(-27.444069, 153.096823);
	    var sw = new google.maps.LatLng(-27.526912, 152.942671);

	    bounds = new google.maps.LatLngBounds(sw, ne);

	    directionsService = new google.maps.DirectionsService();
	}

    return {
    	initGeo: initGeo,
    	getSuggestions: getSuggestions,
    	getDirections: getDirections
    }

})(LUNGO, App);