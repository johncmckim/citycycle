App.Events = (function(lng, app, undefined) {

	lng.dom('#btn-search').tap(function() {
		lng.dom('#search input').show();
    });


	var updateStationButton = function (visible) {
	    var text = visible ? 'Hide' : 'Show'
	    text += ' Stations';
	    lng.dom('#btn-stations').text(text);
	}

	lng.dom('#btn-stations').tap(function () {
	    var visible = stations.toggleMarkers();
	    updateStationButton(visible);
	});

	var results = lng.dom('#search .results');
	
	var search = _.debounce(function (evt) {
	    results.show();
		if(this.value) {

			App.Data.getSuggestions(this.value, function(data) {
				results.html('');

				for(var i = 0; i < data.length; ++i) {
					var item = '<div class="result">' + data[i].formatted_address + '</div>';
					results.append(item);

					var $result = results.children().last();
					$result.data('lat', data[i].geometry.location.lat());
					$result.data('lng', data[i].geometry.location.lng());
				}
			});
		} else {
			results.html('');
		}

	}, 300);

	var getRoute = function (evt) {
	    var to = {
	        lat: $$(this).data('lat'),
	        lng: $$(this).data('lng')
	    }

	    geo.getLocation(function (pos) {
	        var from = {
	            lat: pos.coords.latitude,
	            lng: pos.coords.longitude
	        }

	        var waypoints = [];

	        stations.findClosest(from.lat, from.lng, function (station) {
	            if (isTooFar(station)) {
	                var cont = confirm('It will take ' + station.duration_text + ' to walk from your location to the closet CityCycle station.\nDo you want to continue?');
	                if (!cont) return;
	            }

	            waypoints.push({
	                location: station.location,
	                stopover: true
	            });

	            stations.findClosest(to.lat, to.lng, function (station) {
	                if (isTooFar(station)) {
	                    var cont = confirm('It will take ' + station.duration_text + ' to walk from the last CityCycle station to you destination.\nDo you want to continue?');
	                    if (!cont) return;
	                }

	                waypoints.push({
	                    location: station.location,
	                    stopover: true
	                });

	                App.Data.getDirections(from, to, waypoints, function (directions) {
	                    var visible = stations.setMarkersVisible(false);
	                    updateStationButton(visible);
	                    results.hide();

	                    map.showDirections(directions);
	                });
	            });
	        });
	    });
	};


	var isTooFar = function(station) {
        return (station.duration / 60) >= 20
	}


	var addWaypoint = function (waypoints, station) {
	    waypoints.push({
	        location: new google.maps.LatLng(station.Latitude, station.Longitude),
            stopover: true
	    });
	}

	lng.dom('#search input').on('keyup', search);
	$$('#search .results .result').tap(getRoute);
	lng.dom('a.toHere').tap(getRoute);

    return {

    }

})(LUNGO, App);